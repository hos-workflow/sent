#define DARK "#191A21"
#define LIGHT "#ffffff"

static const char *colors[] = {
	LIGHT, /* foreground color */
	DARK, /* background color */
};

static const char *inverted_colors[] = {
	DARK, /* foreground color */
	LIGHT, /* background color */
};
