#define DARK "#181A1A"
#define LIGHT "#FFFFFF"

static const char *colors[] = {
	LIGHT, /* foreground color */
	DARK, /* background color */
};

static const char *inverted_colors[] = {
	DARK, /* foreground color */
	LIGHT, /* background color */
};
